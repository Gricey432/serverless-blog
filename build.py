"""
Builds a ZIP to upload to Lambda
"""
import importlib
from zipfile import ZipFile


# Define the libraries which should be included
libraries = [
    'markdown',
    'mako',
]

with ZipFile('blog-package.zip', 'w') as blog_zip:
