## Homepage
<%inherit file="base.html.mako" />

<%block name="title">${post.title} - Gricey's Blog</%block>

<%block name="js">
    <script>
$(document).ready(function() {
    // Ensure all external links in posts open in a new tab
    $('.content-container a').filter(function() {
		return this.hostname !== window.location.hostname;
    })
    .each(function() {
        $(this).attr('target', '_blank');
    });
});
</script>
</%block>

<article>
    <h2 class="post-title">${post.title}</h2>
    <section class="post-meta">
        ${post.datetime.strftime('%d %b, %Y')}
    </section>
    <div><a href="/" class="post-back-link">&larr; Back to front page</a></div>
    <section class="post-content">
        ${post.body_html}
    </section>
    <section class="post-comments">
        <div class="fb-comments" data-width="100%" data-numposts="5"></div>
    </section>
</article>
