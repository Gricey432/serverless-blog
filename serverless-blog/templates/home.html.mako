## Homepage
<%inherit file="base.html.mako" />

<%block name="title">Gricey's Blog</%block>

<section class="home-posts">
    % for post in posts:
        <article class="home-post">
            <a href="/${post.url}"><h3>${post.title}</h3></a>
            <div class="post-meta">${post.datetime.strftime('%d %b, %Y')}</div>
        </article>
    % endfor
</section>
