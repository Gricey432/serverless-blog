"""
Holds models for the functionality of the serverless blog
"""
import markdown
import datetime


# Typing
try:
    from typing import Dict, Optional, TypeVar, Union

    T = TypeVar("T")
except ImportError:
    pass


class Request(object):
    """
    Takes an API Gateway request event and turns it into an actual object
    """
    GET = "GET"
    POST = "POST"

    body = None  # type: str
    query_string_paramaters = {}  # type: Dict[str, str]
    http_method = None  # type: str
    headers = {}  # type: Dict[str, str]
    path = None  # type: str

    def __init__(self, event):
        # type: (dict) -> None
        self.body = event.get("body")
        self.query_string_paramaters = event.get("queryStringParameters", {})
        self.http_method = event.get("httpMethod")
        self.headers = event.get("headers", {})
        self.path = event.get("path")  # Empty path is "/"

    def get_query_string_parameter(self, key, default=None):
        # type: (str, Optional[T]) -> Union[str, None, T]
        return self.query_string_paramaters.get(key, default)

    def get_header(self, key, default=None):
        # type: (str, Optional[T]) -> Union[str, None, T]
        return self.headers.get(key, default)

    def is_root_path(self):
        # type: () -> bool
        # Root path is "/"
        return self.path == "/"

    def is_get_request(self):
        # type: () -> bool
        return self.http_method == self.GET

    def is_post_request(self):
        # type: () -> bool
        return self.http_method == self.POST


class Response(object):
    """
    A helper for building a response to send back to API Gateway
    """
    STATUS_OK = "200"

    body = None  # type: str
    status_code = None  # type: int
    headers = {}  # type: Dict[str, str]

    def __init__(self, body, status_code=200):
        # type: (str, int) -> None
        self.body = body
        self.status_code = status_code

        # Default content type
        self.headers = {
            'content-type': "text/html",
        }

    def set_header(self, key, value):
        # type: (str, str) -> None
        self.headers[key] = value

    def serialise(self):
        # type: () -> dict
        """
        Turns the response object into a dictionary to pass back to API Gateway
        """
        return {
            'statusCode': str(self.status_code),
            'body': self.body,
            'headers': self.headers,
        }


class Post(object):
    """
    A Blog Post, stored in a DynamoDB table row
    """
    url = None  # type: str
    timestamp = None  # type: int
    title = None  # type: str
    body = None  # type: str

    _body_html = None  # type: Optional[str]

    def __init__(self, post_item):
        # type: (dict) -> None
        """
        Takes a dynamodb item and turns it into an object
        """
        self.url = post_item.get('url')
        self.timestamp = post_item.get('timestamp')
        self.title = post_item.get('title')
        self.body = post_item.get('body')

    @property
    def body_html(self):
        # type: () -> str
        """
        Gets the Markdown body as a html string
        """
        if not self._body_html:
            self._body_html = markdown.markdown(self.body)
        return self._body_html

    @property
    def datetime(self):
        return datetime.datetime.fromtimestamp(self.timestamp)
