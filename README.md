A really simple, serverless [blog for my site](https://blog.gricey.net).

Using a bunch of managed AWS Services, this blog should be scalable and pretty much free to run.

It relies upon:

* Lambda compute for page generation
* API Gateway for a HTTPs frontend
* DynamoDB for storing posts
* S3 & Cloudfront for static content

Currently any libraries are in the repo with the code, which isn't great. I'll work to remove them soon.